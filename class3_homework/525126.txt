I enjoy the atmosphere during the seminars.
For many git commands I learned what are the best practices while using them.
So far, I already knew most of the commands, but I wasn't always using them in a way that would be considered 'best practice'.
I am looking forward to learning about more advanced ways of using git as well as the lecture on fixing mistakes.
